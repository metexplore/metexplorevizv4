/// <reference types="vite/client" />
declare module "@metabohub/viz-core";
declare module "@metabohub/viz-mapping";
declare module "@metabohub/viz-mask";
declare module "@metabohub/viz-context-menu";
declare module "@metabohub/drop-down-menu";
declare module "@metabohub/viz-style-manager";
declare module "@metabohub/viz-dynamic-data";
declare module "@metabohub/viz-gir";
declare module "@metabohub/viz-metadata-panel";
declare module "@metabohub/info-panel";
declare module "@metabohub/viz-layout";
declare module "@metabohub/viz-force-manager";
declare module "@metabohub/viz-nodelist-panel";
declare module "@metabohub/viz-convexhulls-panel";
declare module "@metabohub/viz-flux";