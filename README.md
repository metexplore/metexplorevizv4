# MetExploreViz4

MetExploreViz4 is an online tool dedicated to the visualization of metabolic networks. It offers a wide range of features for exploring and analyzing these networks. The tool was built using VizCore and its various plugins, which are based on the concept of web components. This modular approach allows for easy addition, replacement, or removal of components, enabling flexible feature management.

## Plugins list

### viz-core

Core of the tool, it manage visualisation. Data management, rendering, styles, etc.

Repository: https://forgemia.inra.fr/metabohub/web-components/viz-core

### viz-style-manager

A panel that allows you to manage the network's style.

Repository: https://forgemia.inra.fr/metabohub/web-components/viz-style-manager

### viz-mapping

A panel that allows you to associate numeric data with the network's style.

Repository: https://forgemia.inra.fr/metabohub/web-components/viz-mapping

### viz-gir

A panel and set of functions that allows to explore network step by step.

Repository: https://forgemia.inra.fr/metabohub/web-components/viz-gir

### viz-dynamic-data

A panel and set of functions that allow you to associate numeric data over time with the network's style and manage its rendering.

Repository: https://forgemia.inra.fr/metabohub/web-components/viz-dynamic-data

### viz-mask

A loading mask.

Repository: https://forgemia.inra.fr/metabohub/web-components/viz-mask

### viz-context-menu

A context menu that can be assigned to an event (such as a right-click) and is modular (with customizable titles and associated functions).

Repository: https://forgemia.inra.fr/metabohub/web-components/viz-context-menu

### drop-down-menu

A modular dropdown menu (with customizable titles and associated functions).

Repository: https://forgemia.inra.fr/metabohub/web-components/drop-down-menu

### convert-to-json

Allows you to convert file formats to JsonGraph.

Repository: https://forgemia.inra.fr/metabohub/web-components/mth-convert-to-json

## Plugin creation

### Project setup

First step is to set up your plugin. You can use our Vue 3 template [template](https://forgemia.inra.fr/metabohub/web-components/vue3-template) or initialise it from scratch. If your plugin will only contain methods, you can also develop a purely TypeScript project. You can find a TypeScript template [here](https://forgemia.inra.fr/metabohub/web-components/ts-template).

### Development

During the development of your plugin, if you need to interact with other plugins and/or VizCore, feel free to consult their respective README files. They contain details about the events and objects they accept and return.

### Deployment 

Once your plugin is developed and tested, the final step is deployment. You need to package and deploy the plugin to an npm repository (public or private). To avoid any issues during deployment, ensure that all key steps work locally. To do this, run the following command lines:

``` bash
npm run test:unit
npm run build
npm pack
```

## Local usage

### Prerequisites

To use the project locally, you need to have Node.js and npm installed.

### Process

#### Project setup

``` bash
npm install
```

#### Run server and create page

``` bash
npm run dev
```